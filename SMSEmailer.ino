#define TINY_GSM_RX_BUFFER 1024  // Set RX buffer to 1Kb

// See all AT commands, if wanted
// #define DUMP_AT_COMMANDS

#include "utilities.h"
#include <TinyGsmClient.h>
#include <ESP_Mail_Client.h>
#include <LinkedList.h>
#include "TinySMS.h"
#include "Arduino.h"
#include <WiFi.h>

#define SMTP_HOST "smtp.gmail.com"
#define SMTP_PORT 587
#define EMAIL "<redacted>@gmail.com"
#define EMAIL_PASSWORD "<redacted>"
#define RECIPIENT_NAME "<redacted>"
#define RECIPIENT_EMAIL "<redacted>@gmail.com"
#define WIFI_SSID "<redacted>"
#define WIFI_PASSWORD "<redacted>"
//#define USE_GPRS

#ifdef DUMP_AT_COMMANDS  // if enabled it requires the streamDebugger lib
#include <StreamDebugger.h>
StreamDebugger debugger(SerialAT, Serial);
TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif
TinySMS modemSMS(modem);
TinyGsmClient modemClient(modem);
SMTPSession smtp;
Session_Config smtpConfig;
LinkedList<String> pdus;
bool readAllMessages = false;

// It depends on the operator whether to set up an APN. If some operators do not set up an APN,
// they will be rejected when registering for the network. You need to ask the local operator for the specific APN.
// APNs from other operators are welcome to submit PRs for filling.
// #define NETWORK_APN     "CHN-CT"             //CHN-CT: China Telecom

void setup() {
  Serial.begin(115200);  // Set console baud rate
  SerialAT.begin(115200, SERIAL_8N1, MODEM_RX_PIN, MODEM_TX_PIN);

#ifdef BOARD_POWERON_PIN
  pinMode(BOARD_POWERON_PIN, OUTPUT);
  digitalWrite(BOARD_POWERON_PIN, HIGH);
#endif

  // Set modem reset pin ,reset modem
  pinMode(MODEM_RESET_PIN, OUTPUT);
  digitalWrite(MODEM_RESET_PIN, !MODEM_RESET_LEVEL);
  delay(100);
  digitalWrite(MODEM_RESET_PIN, MODEM_RESET_LEVEL);
  delay(2600);
  digitalWrite(MODEM_RESET_PIN, !MODEM_RESET_LEVEL);

  pinMode(BOARD_PWRKEY_PIN, OUTPUT);
  digitalWrite(BOARD_PWRKEY_PIN, LOW);
  delay(100);
  digitalWrite(BOARD_PWRKEY_PIN, HIGH);
  delay(100);
  digitalWrite(BOARD_PWRKEY_PIN, LOW);

  // Check if the modem is online
  Serial.println("Start modem...");

  int retry = 0;
  while (!modem.testAT(1000)) {
    Serial.println(".");
    if (retry++ > 10) {
      digitalWrite(BOARD_PWRKEY_PIN, LOW);
      delay(100);
      digitalWrite(BOARD_PWRKEY_PIN, HIGH);
      delay(1000);
      digitalWrite(BOARD_PWRKEY_PIN, LOW);
      retry = 0;
    }
  }
  Serial.println();

  // Check if SIM card is online
  SimStatus sim = SIM_ERROR;
  while (sim != SIM_READY) {
    sim = modem.getSimStatus();
    switch (sim) {
      case SIM_READY:
        Serial.println("SIM card online");
        break;
      case SIM_LOCKED:
        Serial.println("The SIM card is locked. Please unlock the SIM card first.");
        // const char *SIMCARD_PIN_CODE = "123456";
        // modem.simUnlock(SIMCARD_PIN_CODE);
        break;
      default:
        break;
    }
    delay(1000);
  }

#ifdef NETWORK_APN
  Serial.printf("Set network apn : %s\n", NETWORK_APN);
  modem.sendAT(GF("+CGDCONT=1,\"IP\",\""), NETWORK_APN, "\"");
  if (modem.waitResponse() != 1) {
    Serial.println("Set network apn error !");
  }
#endif

  // Check network registration status and network signal status
  int16_t sq;
  Serial.println("Wait for the modem to register with the network.");
  RegStatus status = REG_NO_RESULT;
  while (status == REG_NO_RESULT || status == REG_SEARCHING || status == REG_UNREGISTERED) {
    status = modem.getRegistrationStatus();
    switch (status) {
      case REG_UNREGISTERED:
      case REG_SEARCHING:
        sq = modem.getSignalQuality();
        Serial.printf("[%lu] Signal Quality:%d\n", millis() / 1000, sq);
        delay(1000);
        break;
      case REG_DENIED:
        Serial.println("Network registration was rejected, please check if the APN is correct");
        return;
      case REG_OK_HOME:
        Serial.println("Online registration successful");
        break;
      case REG_OK_ROAMING:
        Serial.println("Network registration successful, currently in roaming mode");
        break;
      default:
        Serial.printf("Registration Status:%d\n", status);
        delay(1000);
        break;
    }
  }

  Serial.printf("Registration Status:%d\n", status);
  delay(1000);

#ifndef USE_GPRS
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to Wi-Fi");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.println();
#else
  if (!modem.enableNetwork()) {
    Serial.println("Enable network failed!");
  }
#endif

  delay(5000);

  String ueInfo;
  modem.getSystemInformation(ueInfo);
  Serial.println("---------------------------------------------");
#ifdef USE_GPRS
  String ip = "(GSM) " + modem.getLocalIP();
#else
  String ip = "(WiFi) " + WiFi.localIP().toString() + " (RSSI: " + WiFi.RSSI() + ")";
#endif
  String systemInfo = "Modem: " + modem.getModemInfo() + "\nCell: " + ueInfo + "\nGSM Signal: " + String(modem.getSignalQuality()) + "\nIP: " + ip;
  Serial.println(systemInfo);
  Serial.println("---------------------------------------------");

  smtpConfig.server.host_name = SMTP_HOST;
  smtpConfig.server.port = SMTP_PORT;
  smtpConfig.login.email = EMAIL;
  smtpConfig.login.password = EMAIL_PASSWORD;
  smtpConfig.login.user_domain = F("127.0.0.1");
#ifdef USE_GPRS
  smtp.setGSMClient(&modemClient, &modem, "", "", "", "");
#endif
  smtp.callback(smtpCallback);
  MailClient.networkReconnect(true);

  modemSMS.newSMSCallback = newSMSCallback;
  modemSMS.newPDUCallback = newPDUCallback;
  modemSMS.newCallCallback = newCallCallback;
  modemSMS.begin();
  modemSMS.readAll(newSMSCallback);
  modemSMS.removeAll();
  Serial.println("Listening for incoming SMS");
  sendEmail("Modem Bootstrapped", systemInfo);
}

void loop() {
  if (readAllMessages) {
    readAllMessages = false;
    modemSMS.readAll(newSMSCallback);
  } else {
    modemSMS.handle();
  }
  delay(1000);
}

void newSMSCallback(SMS sms) {
  Serial.println("---------------------------------------------");
  Serial.println(sms.sender);
  Serial.println(sms.message);
  Serial.println(sms.date);
  Serial.println("---------------------------------------------");
  String subject = "Message from " + sms.sender;
  String pendingPDUs = "";
  for (int i = 0; i < pdus.size(); ++i) {
    pendingPDUs += pdus.get(i) + "\n\n";
  }
  pdus.clear();
  String message = "Sender: " + sms.sender + "\nDate: " + sms.date + "\n\n" + sms.message + "\n\nPending PDUs:\n" + pendingPDUs;
  sendEmail(subject, message);
}

void newPDUCallback(String pdu) {
  Serial.println("---------------------------------------------");
  Serial.println("Got PDU:");
  Serial.println(pdu);
  pdus.add(pdu);
  Serial.println("---------------------------------------------");
}


void newCallCallback(String caller) {
  Serial.println("---------------------------------------------");
  Serial.printf("Call from: %s\n", caller.c_str());
  Serial.println("---------------------------------------------");
  String subject = "Missed Call from " + caller;
  sendEmail(subject, subject);
}

void ensureSMTPConnected() {
  if (!smtp.isLoggedIn()) {
    if (!smtp.connect(&smtpConfig)) {
      Serial.printf("Connection error, Status Code: %d, Error Code: %d, Reason: %s\n", smtp.statusCode(), smtp.errorCode(), smtp.errorReason().c_str());
      return;
    }

    if (!smtp.isLoggedIn()) {
      Serial.println("Error, Not yet logged in.");
    } else {
      if (smtp.isAuthenticated())
        Serial.println("Successfully logged in.");
      else
        Serial.println("Connected with no Auth.");
    }
  }
}

void sendEmail(const String &subject, const String &body) {
  SMTP_Message message;

  message.sender.name = F("SMS Forwarder");
  message.sender.email = EMAIL;
  message.subject = subject.c_str();
  message.addRecipient(RECIPIENT_NAME, RECIPIENT_EMAIL);

  message.text.content = body.c_str();
  message.text.transfer_encoding = F("base64");
  message.text.charSet = F("utf-8");

  for (int i = 0; i < 3; i++) {
    ensureSMTPConnected();
    if (MailClient.sendMail(&smtp, &message, false)) {
      Serial.printf("Attempt #%d Successful\n", i);
      break;
    } else {
      Serial.printf("Attempt #%d Error, Status Code: %d, Error Code: %d, Reason: %s\n", i, smtp.statusCode(), smtp.errorCode(), smtp.errorReason().c_str());
    }
  }

  readAllMessages = true;
}

void smtpCallback(SMTP_Status status) {
  smtp.sendingResult.clear();
}
